data:extend(
{
	{
		type = "item",
		name = "beltswitcher",
		icon = "__BeltSwitch__/graphics/icons/beltswitcher.png",
		flags = {"goes-to-quickbar"},
		subgroup = "belt",
		order = "d[beltswitcher]",
		place_result = "beltswitcher",
		stack_size = 50
	}
}
)