data:extend(
{
	{
		type = "recipe",
		name = "beltswitcher",
		ingredients =
		{	
			{"express-transport-belt", 1},
			{"iron-gear-wheel", 10},
			{"electronic-circuit", 5}
		},
		enabled = "true",
		result = "beltswitcher",
		result_count = 1
	}
}
)
