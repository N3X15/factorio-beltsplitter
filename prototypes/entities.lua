require ("prototypes.transport-belt-pictures")



function Empty()
	return {
			filename = "__BeltSwitch__/graphics/empty.png",
			priority = "extra-high",
			width = 1,
			height = 1
		}
end

data:extend(
{
	{
		type = "inserter",
		name = "beltswitcher",
		icon = "__BeltSwitch__/graphics/icons/beltswitcher.png",
		flags = {"placeable-neutral", "player-creation"},
		minable = {hardness = 0.2, mining_time = 0.3, result = "beltswitcher"},
		max_health = 50,
		corpse = "small-remnants",
		resistances =
		{
			{
				type = "fire",
				percent = 50
			}
		},
		collision_box = {{-0.4, -0.4}, {0.4, 0.4}},
		selection_box = {{-0.5, -0.5}, {0.5, 0.5}},
		working_sound =
		{
			match_progress_to_activity = true,
			sound =
			{
				{
					filename = "__base__/sound/inserter-fast-1.ogg",
					volume = 0.75
				},
				{
					filename = "__base__/sound/inserter-fast-2.ogg",
					volume = 0.75
				},
				{
					filename = "__base__/sound/inserter-fast-3.ogg",
					volume = 0.75
				},
				{
					filename = "__base__/sound/inserter-fast-4.ogg",
					volume = 0.75
				},
				{
					filename = "__base__/sound/inserter-fast-5.ogg",
					volume = 0.75
				}
			}
		},
		collision_box = {{-0.29, -0.29}, {0.29, 0.29}},
		selection_box = {{-0.5, -0.5}, {0.5, 0.5}},
		pickup_position = {0, 200},
		insert_position = {0, 200},
		energy_per_movement = 10000 / 60,
		energy_per_rotation = 0,
		energy_source =
		{
			type = "electric",
			usage_priority = "secondary-input",
			drain = "0kW"
		},
		hand_base_picture = Empty(),
		hand_closed_picture = Empty(),
		hand_open_picture = Empty(),
		hand_base_shadow = Empty(),
		hand_closed_shadow = Empty(),
		hand_open_shadow = Empty(),
		platform_picture =
		{
			sheet = 
			{
				filename = "__BeltSwitch__/graphics/smartsplitter.png",
				priority = "extra-high",
				width = 64,
				height = 44,
			}
		},
		extension_speed = 1,
		rotation_speed = 1,
		filter_count = 0,
		uses_arm_movement = "basic-inserter"
	}--[[,
	{
		type = "transport-belt",
		name = "old-beltswitcher",
		icon = "__BeltSwitch__/graphics/icons/beltswitcher.png",
		flags = {"placeable-neutral", "player-creation"},
		minable = {hardness = 0.2, mining_time = 0.3, result = "beltswitcher"},
		max_health = 50,
		corpse = "small-remnants",
		resistances =
		{
			{
				type = "fire",
				percent = 50
			}
		},
		collision_box = {{-0.4, -0.4}, {0.4, 0.4}},
		selection_box = {{-0.5, -0.5}, {0.5, 0.5}},
		working_sound =
		{
			sound =
			{
				filename = "__base__/sound/express-transport-belt.ogg",
				volume = 0.4
			},
			max_sounds_per_type = 3
		},
		animation_speed_coefficient = 32,
		animations =
		{
			filename = "__base__/graphics/entity/express-transport-belt/express-transport-belt.png",
			priority = "extra-high",
			width = 40,
			height = 40,
			frame_count = 32,
			direction_count = 12
		},
		belt_horizontal = express_belt_horizontal, -- specified in transport-belt-pictures.lua
   		belt_vertical = express_belt_vertical,
    	ending_top = express_belt_ending_top,
    	ending_bottom = express_belt_ending_bottom,
   		ending_side = express_belt_ending_side,
    	starting_top = express_belt_starting_top,
    	starting_bottom = express_belt_starting_bottom,
    	starting_side = express_belt_starting_side,
    	ending_patch = ending_patch_prototype,
    	ending_patch = ending_patch_prototype,
   		fast_replaceable_group = "transport-belt",
   		speed = 0.09375
	}]]--
}
)