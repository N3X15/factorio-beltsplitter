require "defines"
require "utils"

script.on_init(DoSetup)
script.on_load(DoSetup)
script.on_event(defines.events.on_tick, function(event) OnTick(event) end)
script.on_event(defines.events.on_built_entity, function(event) OnBuiltEntity(event) end)
script.on_event(defines.events.on_entity_died, function(event) OnEntityDied(event) end)
script.on_event(defines.events.on_preplayer_mined_item, function(event) OnPrePlayerMinedItem(event) end)


function DoSetup()
	if global.sb == nil then
		global.sb = {
			version = utils.currentVersion,
			switchers = {}
		}
	end
	if global.sb.version ~= utils.currentVersion then
		game.player.print("Migrating BeltSwitchers from version " .. global.sb.version .. " to version " .. utils.currentVersion .. ".")
		utils.migrations[global.sb.version]()
		game.player.print("Migration successful.")
	end
end

function Coords2Direction(target,origin)
	-- north = -y
	-- west = -x
	if target.x > origin.x and target.y == origin.y then
		return defines.direction.east
	elseif target.x < origin.x and target.y == origin.y then
		return defines.direction.west
	elseif target.x == origin.x and target.y < origin.y then
		return defines.direction.north
	elseif target.x == origin.x and target.y > origin.y then
		return defines.direction.south
	else
		return nil
	end
end

function Turn180(direction)
	if direction == defines.direction.north then
		return defines.direction.south
	elseif direction == defines.direction.east then
		return defines.direction.west
	elseif direction == defines.direction.south then
		return defines.direction.north
	elseif direction == defines.direction.west then
		return defines.direction.east
	else
		return nil
	end
end
TOWARDS="towards"
AWAY="away"
NA="n/a"
function GetFacing(target,origin)
	local directionTo = Coords2Direction(target.position,origin.position)
	if directionTo == target.direction then -- facing north, dirTo is north
		return AWAY
	elseif directionTo == Turn180(target.direction) then
		return TOWARDS
	else
		return NA
	end
end
						  
function getAllData(t, prevData)
  -- if prevData == nil, start empty, otherwise start with prevData
  local data = prevData or {}

  -- copy all the attributes from t
  for k,v in pairs(t) do
    data[k] = data[k] or v
  end

  -- get t's metatable, or exit if not existing
  local mt = getmetatable(t)
  if type(mt)~='table' then return data end

  -- get the __index from mt, or exit if not table
  local index = mt.__index
  if type(index)~='table' then return data end

  -- include the data from index into data, recursively, and return
  return getAllData(index, data)
end

function dir2name(dirid)
	if dirid == defines.direction.north then return "NORTH" end
	if dirid == defines.direction.east then return "EAST" end
	if dirid == defines.direction.south then return "SOUTH" end
	if dirid == defines.direction.west then return "WEST" end
	return "??? ("..tostring(dirid)..")"
end

function OnTick(_Event)
	if global.sb == nil then 
		DoSetup()
	end
	for i, switcher in pairs(global.sb.switchers) do
		if switcher.ent.valid then
			--game.player.print(switcher.ent.type)
			--for propertyName, value in pairs(getAllData(switcher.ent)) do
			--	game.player.print("  "..propertyName.."")
			--end
			local behind=nil
			local front=nil
			-- {-0.5, -0.5}, {0.5, 0.5} * 3 = {{-1.5,-1.5},{1.5,1.5}}
			local entpos = switcher.ent.position
			
			switcher.ent.active=false
			
			for i,belt in ipairs(switcher.ent.surface.find_entities_filtered{type="transport-belt", area={{entpos.x-1.0,entpos.y-1.0},{entpos.x+1.0,entpos.y+1.0}}}) do
				--game.player.print(tostring(belt).." = "..tostring(belt.position))
				local dirTo = Coords2Direction(belt.position,switcher.ent.position)
				local facing = GetFacing(belt,switcher.ent)
				--local assignment = tostring(i)
				if facing == TOWARDS and dirTo == Turn180(switcher.ent.direction) then
					behind=belt
					--assignment = "INPUT"
				elseif facing ~= TOWARDS and dirTo == switcher.ent.direction then
					front=belt
					--assignment = "OUTPUT"
				end
				--game.player.print(assignment.." = {x="..tostring(belt.position.x)..",y="..tostring(belt.position.y)..",facing="..facing..",dir="..dir2name(belt.direction)..",dirTo="..dir2name(dirTo).."}")
			end
			--game.player.print("Found behind="..tostring(behind)..", front="..tostring(front))
			
			if behind~=nil and front~=nil then
				for i,laneLeft in pairs({true,false}) do
					local behindLaneID=0
					local frontLaneID=0
					-- I miss ternary operators.
					if laneLeft then
						behindLaneID=1
						frontLaneID=2
					else
						behindLaneID=2
						frontLaneID=1
					end
					
					local behindLane = behind.get_transport_line(behindLaneID)
					local frontLane = front.get_transport_line(frontLaneID)
					
					if behindLane.get_item_count() > 0 then
						for itemName, itemAmount in pairs(behindLane.get_contents()) do
							if frontLane.can_insert_at_back() then
							  local stack = {name = itemName, count = 1} -- This may not work as expected.
							  behindLane.remove_item (stack)
							  frontLane.insert_at_back(stack)
							  return true
							end
						end
					end
				end
			end
		end
	end
end

function OnBuiltEntity(_Event)
	if global.sb == nil then 
		DoSetup()
	end
	if not IsSwitcher(_Event.created_entity) then return end
	
	AddSwitcher(_Event.created_entity)
end

function OnEntityDied(_Event)
	if global.sb == nil then 
		DoSetup()
	end
	if not IsSwitcher(_Event.entity) then return end
	
	RemoveSwitcher(_Event.entity)
end

function OnPrePlayerMinedItem(_Event)
	if global.sb == nil then 
		DoSetup()
	end
	if not IsSwitcher(_Event.entity) then return end
	
	RemoveSwitcher(_Event.entity)
end

local VV = 0.25
local targetOff = {[0] = {left = {x = -0.23, y = -VV  }, right = {x =  0.23, y = -VV  }},
				   [2] = {left = {x =  VV  , y = -0.23}, right = {x =  VV  , y =  0.23}},
				   [4] = {left = {x =  0.23, y =  VV  }, right = {x = -0.23, y =  VV  }},
				   [6] = {left = {x = -VV  , y =  0.23}, right = {x = -VV  , y = -0.23}}
}

function GetScanArea(_Direction, _Position)
  local beltscan_coords = { -- Points to search for transport belts.
    [defines.direction.north] = {
		left={_Position.x - 0.3, _Position.y - 1.1},
		right={_Position.x + 0.3, _Position.y - 0.8}
	},
    [defines.direction.east] = {
		left={_Position.x + 1.1  , _Position.y - 0.3},
		right={_Position.x + 0.8, _Position.y + 0.3}
	},
    [defines.direction.south] = {
		left={_Position.x - 0.3, _Position.y + 1.1},
		right={_Position.x + 0.3, _Position.y + 0.8}
	},
    [defines.direction.west] =  {
		left={_Position.x - 1.1  , _Position.y - 0.3},
		right={_Position.x - 0.8, _Position.y + 0.3}
	}
  }

    if _Direction == defines.direction.north then
      return beltscan_coords[defines.direction.south]
  	elseif _Direction == defines.direction.east then
  	 return beltscan_coords[defines.direction.west]
  	elseif _Direction == defines.direction.south then
  	 return beltscan_coords[defines.direction.north]
  	elseif _Direction == defines.direction.west then
  	 return beltscan_coords[defines.direction.east]
  	else
  	 return {{pos.x , pos.y},{pos.x , pos.y}}
  	end
end

function MoveStackToRight(_Stack, _Switcher)
	newPos = {x = _Switcher.ent.position.x + targetOff[_Switcher.ent.direction].right.x,
			  y = _Switcher.ent.position.y + targetOff[_Switcher.ent.direction].right.y}
	local moved, stuckStack = MoveStack(_Switcher.ent.surface, _Stack, newPos)
	if not moved and stuckStack and stuckStack.valid then
		_Switcher.stuckStack.left = stuckStack
	end
end

function MoveStackToLeft(_Stack, _Switcher)
	newPos = {x = _Switcher.ent.position.x + targetOff[_Switcher.ent.direction].left.x,
			  y = _Switcher.ent.position.y + targetOff[_Switcher.ent.direction].left.y}
	local moved, stuckStack = MoveStack(_Switcher.ent.surface, _Stack, newPos)
	if not moved and stuckStack and stuckStack.valid then
		_Switcher.stuckStack.right = stuckStack
	end
end

function MoveStack(surface, _Stack, _NewPos)
	-- Check if the target location is empty.
	local possiblePos = surface.find_non_colliding_position("item-on-ground", _NewPos, 0.10, 0.01)
	if possiblePos then
		surface.create_entity{position=possiblePos, name="item-on-ground", stack={name=_Stack.stack.name, count=_Stack.stack.count}}
		_Stack.destroy()
											
		return true, nil
	else
		-- Teleport the stack, locking it in position.
		_Stack.teleport(_Stack.position)
		return false, _Stack
	end
	return false, nil
end

function IsSwitcher(entity)
	if not entity or not entity.valid then
		return false
	end
	game.player.print("IsSwitcher: "..entity.name)
	if entity.name == "beltswitcher" then
		return true
	end
	return false
end

function AddSwitcher(_Entity)
	game.player.print("Added beltswitcher.")
	table.insert(global.sb.switchers, {	
		ent = _Entity,
		stuckStack = {left = nil, right = nil}
	})
end

function RemoveSwitcher(_Entity)
	for i, switcher in pairs(global.sb.switchers) do
		if switcher.ent == _Entity then
			table.remove(global.sb.switchers, i)
			break
		end
	end
end
function GetSwitchScanArea(_Direction, _Position)
	local offL = 0.05
	local offH = 0.3
	if _Direction == 0 then
		return {left  = {{_Position.x + -offH, _Position.y        }, {_Position.x       , _Position.y + offL}},
				right = {{_Position.x        , _Position.y        }, {_Position.x + offH, _Position.y + offL}}
			   }
	elseif _Direction == 2 then
		return {left  = {{_Position.x + -offL, _Position.y + -offH}, {_Position.x       , _Position.y       }},
				right = {{_Position.x + -offL, _Position.y        }, {_Position.x       , _Position.y + offH}}
			   }
	elseif _Direction == 4 then
		return {left  = {{_Position.x        , _Position.y + -offL}, {_Position.x + offH, _Position.y       }},
				right = {{_Position.x + -offH, _Position.y + -offL}, {_Position.x       , _Position.y       }}
			   }
	else
		return {left  = {{_Position.x        , _Position.y        }, {_Position.x + offL, _Position.y + offH}},
				right = {{_Position.x        , _Position.y + -offH}, {_Position.x + offL, _Position.y       }}
			   }
	end
end



-- From SmartSwitchers.
function MoveStack12(_ItemName, _Splitter, _SrcLine, _LineI)
  if _Splitter.energy < 1 then
    return false
  end
  
  local facing_directions = {[north] = south, [east] = west, [south] = north, [west] = east}
  local scanArea = GetScanArea(facing_directions[_Splitter.direction], _Splitter.position)

--[[   
  local found=false
  for i = 1, 5 do
    local itemName = _Splitter.get_filter(i)
    if itemName and itemName == _ItemName then
      found=true
    end
  end
  
  if not found then
    return
  end
--]]
    
  local belt=_Splitter.surface.find_entities_filtered{type = "transport-belt", area = scanArea}
  if belt[1] ~= nil and belt[1].direction == _Splitter.direction then
--    debugLog(string.format("nws ms12 dir=%s wd=%s pos=%s sa=%s bp=%s",_Splitter.direction,facing_directions[_Splitter.direction],table.tostring(_Splitter.position),table.tostring(scanArea),table.tostring(belt[1].position)))
    -- Check if the target location is empty.
    local tgtLane=belt[1].get_transport_line(_LineI)
    if tgtLane.can_insert_at_back() then
      local stack = {name = _ItemName, count = 1}
      _SrcLine.remove_item (stack)
      tgtLane.insert_at_back(stack)
      return true
    end
    return false
  end
end
